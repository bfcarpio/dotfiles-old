#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export BROWSER="firefox"
export EDITOR="vim"
export TERMINAL="kitty"
export READER="zathura"
export FILE="ranger"

export BORG_REPO="/run/media/bfcarpio/easystore/Borg Backups"

[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x i3 > /dev/null && exec startx
